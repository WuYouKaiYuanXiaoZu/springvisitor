# Spring Visitor

SpringVisitor是一个轻量级的Spring访问层框架。

它提供了从Controller层到Service层的自动路由：比如url模式为/xxxx/{service}/{method}，
则只需要编写一个通用controller便可以将该url下的http请求路由到对应service的对应方法中，
不再因为service层的扩展而编写更多模板化的controller类。


## 特性
  - Controller层到Service层的自动关联
  - 方法级别的缓存，适用于重复请求较多的场景
  - 统一的HttpResponse处理和异常处理
  - 更加简洁的Interceptor编写
  - 基于ARestfulService快速开发Restful风格的增删改查接口

## 配置要求
  - JDK1.8或以上
  - SpringMVC 4.x

## 导入

在你的spring项目的pom文件中，添加如下Maven依赖:

```xml
<dependencies>
    <dependency>
        <groupId>com.yunchang</groupId>
        <artifactId>springvisitor-core</artifactId>
        <version>0.5.0</version>
    </dependency>
</dependencies>

<distributionManagement>
    <repository>
        <id>springvisitor-releases</id>
        <name>SpringVisitor Repository</name>
        <url>http://testssdk.sail2world.com:8089/repository/releases/</url>
    </repository>
</distributionManagement>
```


## 如何使用

1. 创建一个自定义的Controller，并继承AReflectiveController。添加一个自定义方法，url模式中包含service和method，如下：

```java
@Controller
public class XXXController extends AReflectiveController {

    @RequestMapping("/xxx/{service}/{method}")
    @ResponseBody
    public Object doRequest(@PathVariable final String service, @PathVariable final String method,
                            final HttpServletRequest request, final HttpServletResponse response) {
        return invokeService(request, response, service, method);
    }
}
```

2. 然后在自己的业务逻辑service类，添加@ReflectiveService注解；在对应的方法中，添加@ReflectiveMethod注解。表明该Service的那些方法可以自动反射调用。

```java
@ReflectiveService("YourServiceName")
@Service
public class XXXService {

    @ReflectiveMethod
    public String xxxMethod(Map<String, String> params) {
        ...
        return "";
    }

    // 该接口接收GET请求或POST表单请求
    @ReflectiveMethod(params = {"aa", "bb"})
    public String xxxMethod2(String aa, String bb) {
        ...
        return "";
    }

    // 该接口接收POST方式的application/json
    @ReflectiveMethod(contentType = ContentType.JSON)
    public String xxxMethod3(String json) {
        ...
        return "";
    }
}
```

3. 自定义统一的HttpRequest处理器、HttpResponse处理器和异常处理器

```java
@Component
public class MyRequestHandler extends ABaseRequestHandler {
    @Override
    public Map<String, String> parseToMap(String httpContent, ContentType contentType) throws Exception {
        // 如果支持application/json的请求，则需要将InputStream的字符串内容转化为Map<String, String>对象(比如使用fastjson等第三方库)。
        // 如果不支持application/json的请求，则直接返回null，或抛出异常即可。
        return null;
    }
}
```

```java
@Component
public class MyResponseHandler implements IResponseHandler {
    @Override
    public Object handleResponse(HttpServletRequest request, HttpServletResponse response,
                                 String service, String method, long costTime, Object result,
                                 Map<String, String> paramMap, String httpContent) {
        // 比如统一返回内容为json格式，成功status为ok，失败status为fail
        return result;
    }
}
```

```java
@Component
public class MyExceptionHandler implements IExceptionHandler {
    @Override
    public Object handleError(HttpServletRequest request, HttpServletResponse response, long costTime,
                              Exception exception, Map<String, String> paramMap, String httpContent) {
        // 比如统一返回内容为json格式，status为fail
    }
}
```

4. 在项目的spring配置文件xml中，添加如下配置:

```xml
<!--使用默认的service路由器，可以替换成自己的实现ServiceInvoker-->
<bean id="defaultInvoker" class="com.yunchang.spring.visitor.core.invoker.DefaultServiceInvoker">
    <property name="cache">
        <!--设置默认的内存缓存，也可以不设置缓存-->
		<bean class="com.yunchang.spring.visitor.core.cache.DefaultMapCache"/>
	</property>
</bean>

<!--设置默认的RequestHandler，处理请求参数-->
<bean id="defaultRequestHandler" class="com.xxxx.xxxxx.MyRequestHandler"/>

<!--使用自定义的HttpResponse处理器，处理返回内容-->
<bean id="defaultResponseHandler" class="com.xxxx.xxxxx.MyResponseHandler"/>

<!--使用自定义的异常处理器，处理返回内容-->
<bean id="defaultExceptionHandler" class="com.xxxx.xxxxx.MyExceptionHandler"/>
```

5. 测试一下

在浏览器输入http://127.0.0.1:8080/xxx/YourServiceName/xxxMethod?aa=123&bb=789

该请求即可路由到XXXService的xxxMethod方法了


## 示例

可以运行项目中的demo项目，对Spring Visitor进行调试。

demo中实现了
  - Json格式的HttpResponse处理器和异常处理器
  - 常用的签名验证和白名单验证的Interceptor。
  - 使用DownloadHandler提供文件下载接口
  - 使用默认的DefaultMapCache，并提供了更优秀的CaffeineCache实现
  - 自定义参数类型转化，支持在service的方法中接收自定义类型的参数MyDataType

你可以在自己项目中实现自定义的ServiceInvoker和自定义的缓存ICache，并在spring配置文件中指定成自己的实现。


## 开源许可证

[Mulan PSL](LICENSE)

## 作者

  - jasontujun (jasontujun@gmail.com)
  - duanhao
  - zachary