package com.yunchang.spring.visitor.core.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 访问结果缓存。
 * 用于添加到方法、类上，标识该类/方法的返回结果是需要缓存的。
 *
 * @auther zachary
 * @date 2018/1/22
 * @time 10:18
 * @since 1.0
 */
@Documented
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ReflectiveCache {

    /***
     * 缓存的单位，默认为分钟
     * @return 缓存单位
     */
    TimeUnit unit() default TimeUnit.MINUTES;

    /***
     * 缓存的过期时间，默认为-1，表示永不过期
     * @return 缓存时间
     */
    int expire() default -1;

    /***
     * 不参与生成缓存key的字段列表，比如时间戳等字段不作为缓存key的一部分。
     * @return 数组
     */
    String[] exclude() default {};
}
