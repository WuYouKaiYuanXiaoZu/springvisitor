package com.yunchang.spring.visitor.core.annotation;

import java.lang.annotation.*;

/**
 * 标注restful风格的增删改查接口的访问控制
 * 注意，如果没有此注解 或者使用默认值时，表示对应方法允许访问
 * <p>
 * 例：
 * 某IRestfulService实现类 添加注解 @Restful(list=false),表示该类的list方法被禁用
 * Created by duan on 2018/5/29.
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Restful {
    /**
     * 是否允许list方法。默认为true，允许
     */
    boolean list() default true;

    /**
     * 是否允许add方法。默认为true，允许
     */
    boolean add() default true;

    /**
     * 是否允许remove方法。默认为true，允许
     */
    boolean remove() default true;

    /**
     * 是否允许get方法。默认为true，允许
     */
    boolean get() default true;

    /**
     * 是否允许update方法。默认为true，允许
     */
    boolean update() default true;
}
