package com.yunchang.spring.visitor.core.service;

import java.util.Map;

/**
 * 对数据进行增删改查服务的接口
 * 子接口实现该接口 并添加注解 ReflectiveService
 */
public interface IRestfulService {

    /**
     * 根据查询条件，返回所有查询结果(根据入参params，决定是否支持分页)
     * @param params 入参(查询字段条件)
     * @return 返回查询结果(非分页查询,返回list.分页查询,返回类型自定义)
     * @throws Exception
     */
    Object list(Map<String, String> params) throws Exception;

    /**
     * 根据主键id，查询单个数据
     * @param id 主键id
     * @param params 入参
     * @return 返回查询结果
     * @throws Exception
     */
    Map get(String id, Map<String, String> params) throws Exception;

    /**
     * 添加数据
     * @param params 入参
     * @return 返回新增数据
     * @throws Exception
     */
    Map add(Map<String, String> params) throws Exception;

    /**
     * 根据主键id，删除数据
     * @param id 主键id
     * @param params 入参
     * @return 返回删除的数据
     * @throws Exception
     */
    Map remove(String id, Map<String, String> params) throws Exception;

    /**
     * 根据主键id，更新数据
     * @param id 主键id
     * @param params 入参
     * @return 返回修改后的数据
     * @throws Exception
     */
    Map update(String id, Map<String, String> params) throws Exception;
}
