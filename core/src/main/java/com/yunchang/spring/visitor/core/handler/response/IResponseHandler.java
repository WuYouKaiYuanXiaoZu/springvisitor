package com.yunchang.spring.visitor.core.handler.response;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Http返回格式处理的接口
 * Created by jasontujun on 2018/1/16.
 */
public interface IResponseHandler {

    /**
     * 对请求返回结果的自定义处理（比如对返回的格式进行处理等）.
     *
     * @param request http请求
     * @param response http返回
     * @param service service名
     * @param method 方法名
     * @param costTime 请求处理耗时
     * @param result 如果调用成功，result是调用service对应方法的返回值；
     * @param params 入参（GET/POST表单等请求，入参可能有值）
     * @param httpContent 入参（POST JSON/XML等请求，入参可能有值）
     * @return 返回最终的http返回内容
     * @throws Exception 如果调用过程中抛出异常，则result为空，exception是抛出的异常。
     */
    Object handleResponse(HttpServletRequest request,
                          HttpServletResponse response,
                          String service,
                          String method,
                          long costTime,
                          Object result,
                          Map<String, String> params,
                          String httpContent) throws Exception;
}
