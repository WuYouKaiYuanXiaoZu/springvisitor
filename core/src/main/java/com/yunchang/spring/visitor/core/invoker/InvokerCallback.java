package com.yunchang.spring.visitor.core.invoker;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 反射调用service方法的回调
 * Created by jasontujun on 2019/9/18.
 */
public interface InvokerCallback {

    /**
     *
     * @param request
     * @param namespace
     * @param serviceName
     * @param methodName
     * @param result
     * @param params
     * @param httpContent
     */
    void onResponse(HttpServletRequest request, String namespace, String serviceName, String methodName,
                    Object result, Map<String, String> params, String httpContent) throws Exception;

    /**
     * 反射调用出现异常
     * @param request
     * @param namespace 命名空间区分各个service
     * @param serviceName Service名字
     * @param methodName 方法名
     * @param e 异常
     * @param params 参数
     * @param httpContent
     */
    void onException(HttpServletRequest request, String namespace, String serviceName, String methodName,
                     Exception e, Map<String, String> params, String httpContent);
}
