package com.yunchang.spring.visitor.core.annotation;

import com.yunchang.spring.visitor.core.constant.ContentType;

import java.lang.annotation.*;

/**
 * 标注可以用于反射的方法<br/>
 * 注意，同一个类中，不允许相同的方法名（包括别名）!
 * <p>
 * 使用该注解声明的方法，需遵守如下约定：
 * 1. 如果声明的方法无参数，直接在方法上添加注解即可 （无需配置params属性）
 * 2. 如果声明的方法只有一个Map类型的参数，直接在方法上添加注解即可 （无需配置params属性）
 * 3. 如果方法声明的参数中包含如下类型，直接在方法上添加注解即可 （无需配置params属性）
 *    HttpMethod、InputStream、UrlParamsMap、HeaderParamsMap【顺序可乱】
 * 4，如果方法声明的参数，包含除以上说明的其他参数，则在方法上添加注解时，需配置params属性。
 *    用于指定http请求的入参名与方法参数的映射关系，这种映射关系是有序的。
 *
 * Created by jasontujun on 2018/1/10.
 */
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ReflectiveMethod {

    /**
     * 标明方法的别名。如果不指定别名，默认为方法声明时的名字。
     *
     * @return 返回方法的别名；如果返回为空，则使用方法声明时的名字
     */
    String alias() default "";

    /**
     * 标明方法接收的参数名列表
     *
     * @return 如果返回值不为空，则表示方法入参名字列表(必须和方法定义的参数列表顺序一致)。<br/>
     * 如果返回值为空列表，则方法的参数必须有且只有一个，且为Map<String,String>类型
     */
    String[] params() default {};

    /**
     * 标明方法接收的内容类型ContentType，默认为application/x-www-form-urlencoded
     * @return 返回ContentType类型
     */
    ContentType contentType() default ContentType.FORM;
}
