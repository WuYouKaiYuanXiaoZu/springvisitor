package com.yunchang.spring.visitor.core.paramtype;

import java.util.HashMap;
import java.util.Map;

/**
 * 存请求header中的参数
 */
public class HeaderParamsMap extends HashMap<String, String> {

    public HeaderParamsMap() {
        super();
    }

    public HeaderParamsMap(Map<String,String> map) {
        this.putAll(map);
    }
}