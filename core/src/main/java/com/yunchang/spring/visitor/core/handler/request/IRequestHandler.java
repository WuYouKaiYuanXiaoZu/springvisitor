package com.yunchang.spring.visitor.core.handler.request;

import com.yunchang.spring.visitor.core.constant.ContentType;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 处理requst请求中的参数
 * Created by jasontujun on 2019/2/15.
 */
public interface IRequestHandler {

    /**
     * 获取请求的参数(不含url中的参数，不含header中的参数)，处理GET/POST表单的请求
     *
     * @param request 请求
     * @return 返回请求的参数
     */
    Map<String, String> getParams(HttpServletRequest request);

    /**
     * 获取请求的InputStream的内容，处理POST JSON/XML的请求。
     * 注意：该方法可能被多次调用，但一个InputStream对象只能读一次，需要在该请求声明周期内缓存InputStream的内容，并在请求返回后，使缓存失效。
     * @param request 请求
     * @return 返回请求的内容
     */
    String getHttpContent(HttpServletRequest request);

    /**
     * 将请求的InputStream的内容(处理POST JSON/XML的请求时)，转化为Map<String, String>对象
     * @param httpContent InputStream的内容(字符串)
     * @param contentType 内容类型
     * @see ContentType
     * @return 如果转化成功，返回Map<String, String>对象
     * @throws Exception 如果不支持转化，或转化失败，可以抛出异常
     */
    Map<String, String> parseToMap(String httpContent, ContentType contentType) throws Exception;
}
