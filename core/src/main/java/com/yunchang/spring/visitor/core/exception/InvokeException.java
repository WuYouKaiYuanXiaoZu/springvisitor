package com.yunchang.spring.visitor.core.exception;

/**
 * 方法反射调用异常
 * Created by jasontujun on 2019/9/18.
 */
public class InvokeException extends RuntimeException {

    public InvokeException() {
        super();
    }

    public InvokeException(String message) {
        super(message);
    }
}
