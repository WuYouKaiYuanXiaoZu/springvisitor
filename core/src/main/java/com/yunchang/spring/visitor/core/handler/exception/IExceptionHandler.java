package com.yunchang.spring.visitor.core.handler.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Http请求出现异常时的返回内容处理的接口。
 * Created by jasontujun on 2018/1/16.
 */
public interface IExceptionHandler {

    /**
     * 对请求的异常处理.
     *
     * @param request   http请求
     * @param response  http返回
     * @param exception 异常
     * @param costTime 请求处理耗时
     * @param paramMap 入参（GET/POST表单等请求，入参可能有值）
     * @param httpContent 入参（POST JSON/XML等请求，入参可能有值）
     * @return 返回处理的结果
     */
    Object handleError(HttpServletRequest request,
                       HttpServletResponse response,
                       long costTime,
                       Throwable exception,
                       Map<String, String> paramMap,
                       String httpContent);

    /**
     * 将handleError()返回的结果转换为字符串
     *
     * @param response 处理结果
     * @return 返回字符串
     */
    String response2string(Object response);
}
