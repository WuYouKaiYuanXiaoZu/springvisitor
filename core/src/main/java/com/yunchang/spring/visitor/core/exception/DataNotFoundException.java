package com.yunchang.spring.visitor.core.exception;

/**
 * springvisitor中 DAO层 DataNotFound异常
 * Created by duan on 2018/5/24.
 */
public class DataNotFoundException extends RuntimeException {
    public DataNotFoundException() {
        super();
    }

    public DataNotFoundException(String message) {
        super(message);
    }
}
