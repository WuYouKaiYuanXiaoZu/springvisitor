package com.yunchang.spring.visitor.core.annotation;

import java.lang.annotation.*;

/**
 * 标注反射的Service的名字。
 * 由namespace+value确定唯一的service对象。
 * input表示入参过滤
 * outPut表示返参过滤，如果返参是Map  或者 List<Map> 类型，那么会根据配置的注解字段进行过滤。
 * <p>
 * Created by jasontujun on 2018/1/10.
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ReflectiveService {

    /**
     * service名(必填)
     */
    String value();

    /**
     * 命名空间(选填，默认为空)
     */
    String namespace() default "";

    //输入参数过滤
    Params[] input() default {};

    //输出参数过滤
    Params[] output() default {};

}
