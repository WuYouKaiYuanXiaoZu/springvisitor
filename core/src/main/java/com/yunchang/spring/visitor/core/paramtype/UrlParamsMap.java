package com.yunchang.spring.visitor.core.paramtype;

import java.util.HashMap;
import java.util.Map;

/**
 * 请求url中的参数列表
 */
public class UrlParamsMap extends HashMap<String, String> {

    public UrlParamsMap() {
        super();
    }

    public UrlParamsMap(Map<String,String> map) {
        this.putAll(map);
    }
}