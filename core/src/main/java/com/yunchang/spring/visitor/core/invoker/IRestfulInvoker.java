package com.yunchang.spring.visitor.core.invoker;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 反射调用Service方法的Invoker接口，并对注解ServiceMethod，ServiceParam进行判断+过滤
 * Created by jasontujun on 2018/1/10.
 */
public interface IRestfulInvoker {

    /**
     * 调用@ReflectiveService值为${service}的Service的对应方法。
     * 该方法必须被@ReflectiveMethod标注了，否则无法反射调用该方法，抛出IllegalArgumentException
     *
     * @param namespace   命名空间区分各个service
     * @param serviceName Service名字
     * @param id          数据的id
     * @param request     请求
     * @return 返回调用对应方法的返回值
     * @throws IllegalArgumentException 当Service或者Method名字不存在,或方法没有@ReflectiveMethod注解时抛出
     * @see com.yunchang.spring.visitor.core.annotation.ReflectiveService
     * @see com.yunchang.spring.visitor.core.annotation.ReflectiveMethod
     */
    Object invokeRestful(String namespace, String serviceName, String id, HttpServletRequest request,
                         Map<String, String> params) throws Exception;
}
