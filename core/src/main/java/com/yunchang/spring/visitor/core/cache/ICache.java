package com.yunchang.spring.visitor.core.cache;

import com.yunchang.spring.visitor.core.annotation.ReflectiveCache;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 缓存层的接口
 * @auther zachary
 * @date 2018/1/22
 * @time 10:34
 */
public interface ICache {

    /**
     * 根据Service名字+方法名+参数Map(GET/POST表单的请求)，生成该次请求的唯一的key
     * @param service Service名字
     * @param method 方法名
     * @param params 所有的参数Map
     * @param excludes 不参与缓存的字段列表
     * @return 返回生成的唯一的key；如果返回为null，则后续不会进行缓存。
     * @see ReflectiveCache
     */
    String generateKey(String service, String method, Map<String, String> params, String[] excludes);

    /**
     * 根据Service名字+方法名+httpContent(POST JSON或XML的请求)，生成该次请求的唯一的key
     * @param service Service名字
     * @param method 方法名
     * @param httpContent InputStream转为String的内容
     * @return 返回生成的唯一的key；如果返回为null，则后续不会进行缓存。
     * @see ReflectiveCache
     */
    String generateKey(String service, String method, String httpContent);

    /***
     * 根据key查询对应的缓存
     * @param key 缓存的key
     * @return 返回对应key的缓存；返回null，表示没有缓存
     */
    Object get(String key);

    /**
     * 将key-value缓存起来
     * @param key 缓存的key
     * @param value 缓存的值
     * @param expire 过期时间，默认为-1，表示用不过期
     * @param unit 过期时间单位，默认为分钟
     * @return 返回true，表示缓存成功；false表示缓存失败。
     * @see ReflectiveCache
     */
    boolean set(String key, Object value, int expire, TimeUnit unit);
}
