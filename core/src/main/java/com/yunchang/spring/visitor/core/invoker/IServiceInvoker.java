package com.yunchang.spring.visitor.core.invoker;

import com.yunchang.spring.visitor.core.handler.request.IRequestHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * 反射调用Service方法的Invoker接口
 * Created by jasontujun on 2018/1/10.
 */
public interface IServiceInvoker {

    /**
     * 调用@ReflectiveService值为${service}的Service的对应方法。
     * 该方法必须被@ReflectiveMethod标注了，否则无法反射调用该方法，抛出IllegalArgumentException
     *
     * @param namespace        命名空间区分各个service
     * @param serviceName      Service名字
     * @param methodName       方法名
     * @param request          请求
     * @param requestHandler  请求处理器
     * @param callback         调用结果的回调函数
     * @throws IllegalArgumentException 当Service或者Method名字不存在,或方法没有@ReflectiveMethod注解时抛出
     * @see com.yunchang.spring.visitor.core.annotation.ReflectiveService
     * @see com.yunchang.spring.visitor.core.annotation.ReflectiveMethod
     */
    void invoke(String namespace, String serviceName, String methodName, HttpServletRequest request,
                  IRequestHandler requestHandler, InvokerCallback callback);
}
