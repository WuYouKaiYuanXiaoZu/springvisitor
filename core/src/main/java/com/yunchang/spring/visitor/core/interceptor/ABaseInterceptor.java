package com.yunchang.spring.visitor.core.interceptor;

import com.yunchang.spring.visitor.core.handler.exception.IExceptionHandler;
import com.yunchang.spring.visitor.core.handler.request.IRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 拦截器基类(封装了异常处理)
 * Created by jasontujun on 2018/1/10.
 */
public abstract class ABaseInterceptor implements HandlerInterceptor {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private IExceptionHandler defaultExceptionHandler;

    @Resource
    private IRequestHandler defaultRequestHandler;

    /**
     * 获取请求的参数（经过IRequestHandler处理后的参数）。
     * 子类可以使用此方法获取请求的参数。
     *
     * @param request 请求
     * @return 返回请求参数
     * @see IRequestHandler
     */
    protected Map<String, String> getParams(HttpServletRequest request) {
        return defaultRequestHandler.getParams(request);
    }

    /**
     * 获取请求的InputStream内容（经过IRequestHandler处理后的参数）。
     * 子类可以使用此方法获取请求的InputStream流内容。
     *
     * @param request 请求
     * @return 返回请求参数
     * @see IRequestHandler
     */
    protected String getHttpContent(HttpServletRequest request) {
        return defaultRequestHandler.getHttpContent(request);
    }

    /**
     * 拦截器前置处理。
     * (该方法将在请求处理之前进行调用，只有该方法返回true，才会继续执行后续的Interceptor和Controller)
     * 默认返回true，子类可以覆盖该方法，自定义前置处理逻辑。
     *
     * @param request
     * @param response
     * @param handler
     * @return
     */
    protected boolean preHandleRequest(HttpServletRequest request,
                                       HttpServletResponse response,
                                       Object handler) throws Exception {
        return true;
    }

    /**
     * 拦截器渲染前处理。
     * (该方法将在请求处理之后，DispatcherServlet进行视图返回渲染之前进行调用)
     * 子类可以覆盖该方法，自定义渲染前处理逻辑。
     *
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     */
    protected void postHandleRequest(HttpServletRequest request,
                                     HttpServletResponse response,
                                     Object handler, ModelAndView modelAndView) throws Exception {
    }

    /**
     * 拦截器后置处理。
     * (该方法将在整个请求结束之后，也就是在DispatcherServlet 渲染了对应的视图之后执行)
     * 子类可以覆盖该方法，自定义后置处理逻辑。
     *
     * @param request
     * @param response
     * @param handler
     * @param e
     */
    protected void afterCompleteRequest(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Object handler, Exception e) throws Exception {
    }


    /**
     * 默认以字符串形式返回给客户端。
     * 子类可以覆盖该方法，以其他形式返回给客户端。
     *
     * @param response HttpServletResponse
     * @param data     返回的内容
     */
    protected void responseError(HttpServletResponse response, Object data) {
        if (data == null) {
            return;
        }
        try {
            String dataStr;
            if (data instanceof String) {
                dataStr = data.toString();
            } else {
                dataStr = defaultExceptionHandler.response2string(data);
            }
            if (dataStr != null) {
                // 强制要求客户端关闭此http连接
                response.setHeader("Connection", "close");
                // 设置返回内容类型json
                response.setContentType("application/json;charset=UTF-8");
                response.getOutputStream().write(dataStr.getBytes("UTF-8"));//使用OutputStream流向客户端输出字节数组
            }
        } catch (IOException e) {
            logger.error("response error", e);
        }
    }

    @Override
    public final boolean preHandle(HttpServletRequest request,
                                   HttpServletResponse response,
                                   Object handler) throws Exception {
        try {
            return preHandleRequest(request, response, handler);
        } catch (Throwable e) {
            logger.error("", e);
            responseError(response, defaultExceptionHandler.handleError(request, response, 0, e, getParams(request), getHttpContent(request)));
            return false;
        }
    }

    @Override
    public final void postHandle(HttpServletRequest request,
                                 HttpServletResponse response,
                                 Object handler, ModelAndView modelAndView) throws Exception {
        try {
            postHandleRequest(request, response, handler, modelAndView);
        } catch (Throwable e) {
            logger.error("", e);
            // 后置拦截处理，无法再次获取InputStream流里的内容
            responseError(response, defaultExceptionHandler.handleError(request, response, 0, e, getParams(request), null));
        }
    }

    @Override
    public final void afterCompletion(HttpServletRequest request,
                                      HttpServletResponse response,
                                      Object handler, Exception e) throws Exception {
        try {
            afterCompleteRequest(request, response, handler, e);
        } finally {
            // 请求最终有异常，才会打印格式化日志
            if (e != null) {
                // 后置拦截处理，无法再次获取InputStream流里的内容
                responseError(response, defaultExceptionHandler.handleError(request, response, 0, e, getParams(request), null));
            }
        }
    }
}
