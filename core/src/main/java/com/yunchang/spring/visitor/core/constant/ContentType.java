package com.yunchang.spring.visitor.core.constant;

/**
 * Http内容类型的枚举。
 * Created by jasontujun on 2019/9/17.
 */
public enum  ContentType {
    FORM,// 普通表单
    JSON,
    XML;
}
