package com.yunchang.spring.visitor.core.annotation;

import java.lang.annotation.*;

/**
 * 用于类，表示该类的某个methodName方法允许的参数是params
 *
 * Created by duan on 2018/5/29.
 */

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Params {
    //方法名字
     String methodName() default "";

    //方法允许的参数
     String[] params() default {};
}
