package com.yunchang.spring.visitor.core.cache;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * <pre>
 * 基于LinkedHashMap实现的简单IServiceCache缓存实现类。
 *      - 最大缓存数量可配置(小于等于0，表示没有数量限制)
 *      - 支持LRU的淘汰策略。
 *      - 支持过期时间
 * </pre>
 * Created by jasontujun on 2018/1/22.
 */
public class DefaultMapCache implements ICache {

    // 缓存map
    private CacheMap<String, TimeStampEntry> cacheMap;

    // 默认缓存大小无限制。可配置成自定义值
    private int maxSize = 0;

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    public String generateKey(String service, String method, Map<String, String> params, String[] excludes) {
        String result = "[cache]" + service + "." + method;
        if (params == null || params.size() == 0) {
            return result;
        }
        for (String exclude : excludes) {
            params.remove(exclude);
        }
        TreeMap<String, String> sortMap = new TreeMap<String, String>(params);
        for (String key : sortMap.keySet()) {
            result = result + "." + key + "=" + sortMap.get(key);
        }
        return result;
    }

    @Override
    public String generateKey(String service, String method, String httpContent) {
        return "[cache]" + service + "." + method + httpContent;
    }

    @Override
    public synchronized Object get(String key) {
        if (cacheMap == null) {
            return null;
        }
        TimeStampEntry entry = cacheMap.get(key);
        if (entry == null) {
            return null;
        }
        if (entry.timeout()) {
            // 如果entry过期，则删除
            cacheMap.remove(key);
            return null;
        }
        return entry.realEntry;
    }

    @Override
    public synchronized boolean set(String key, Object value, int expire, TimeUnit unit) {
        if (cacheMap == null) {
            cacheMap = new CacheMap<String, TimeStampEntry>(maxSize);
        }
        cacheMap.put(key, new TimeStampEntry(value, expire, unit));
        return true;
    }

    /**
     * 大小为maxSize的支持LRU的Map
     */
    private static class CacheMap<K, V> extends LinkedHashMap<K, V> {

        private final int maxSize;

        public CacheMap(int maxSize) {
            super((maxSize <= 0 || maxSize > 32) ? 32 : maxSize, 0.75f, true);
            this.maxSize = maxSize;
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry eldest) {
            return size() > maxSize;
        }
    }

    private static class TimeStampEntry {
        private long expireTimeStamp;
        private Object realEntry;

        public TimeStampEntry(Object obj, int expire, TimeUnit unit) {
            if (expire > 0) {
                this.expireTimeStamp = System.currentTimeMillis() + unit.toMillis(expire);
            }
            this.realEntry = obj;
        }

        public boolean timeout() {
            return expireTimeStamp > 0 && System.currentTimeMillis() > expireTimeStamp;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof TimeStampEntry && realEntry.equals(((TimeStampEntry) obj).realEntry);
        }

        @Override
        public int hashCode() {
            return realEntry.hashCode();
        }
    }
}
