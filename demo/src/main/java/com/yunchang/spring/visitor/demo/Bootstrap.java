package com.yunchang.spring.visitor.demo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * 启动Api项目主入口
 * <pre>
 * Created with IntelliJ IDEA.
 * User: zachary.
 * Date: 2014/7/28
 * Time: 16:16
 * PC：windows'IDEA in company <br>
 * </pre>
 *
 * @author zachary.
 */
public class Bootstrap {

    public static void main(String[] args) throws IOException {

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("jetty.xml");
        context.start();
    }
}
