package com.yunchang.spring.visitor.demo.controller;

import com.yunchang.spring.visitor.core.controller.AReflectiveController;
import com.yunchang.spring.visitor.core.handler.response.IResponseHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 测试Controller
 * Created by jasontujun on 2018/1/11.
 */
@Controller
public class TestController extends AReflectiveController {

    @Resource
    private IResponseHandler downloadHandler;

    @RequestMapping("/test")
    @ResponseBody
    public Object test(final HttpServletRequest request, final HttpServletResponse response) {
        response.setContentType("text/plain;charset=UTF-8");
        response.setHeader("Connection", "close");
        return "ok";
    }

    @RequestMapping("/download/{service}/{method}")
    @ResponseBody
    public Object doDownload(@PathVariable final String service, @PathVariable final String method,
                             final HttpServletRequest request, final HttpServletResponse response) {
        return invokeService(request, response, "download", service, method, downloadHandler);
    }

    @RequestMapping("/{namespace}/{service}/{method}")
    @ResponseBody
    public Object doTest(@PathVariable final String namespace, @PathVariable final String service, @PathVariable final String method,
                         final HttpServletRequest request, final HttpServletResponse response) {
        return invokeService(request, response, namespace, service, method);
    }
}
