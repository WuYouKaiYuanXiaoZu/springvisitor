package com.yunchang.spring.visitor.demo.cache;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.yunchang.spring.visitor.core.cache.ICache;
import javafx.util.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * 自定义缓存示例：使用Caffeine作为请求缓存的实现。
 *
 * @auther zachary
 * @date 2018/1/22
 * @time 11:19
 */
public class CaffeineCache implements ICache {

    /***
     * 过期时间对应的cache
     */
    private final Map<String, Cache<String, Object>> EXPIRE_CACHE = new HashMap<>();
    /***
     * 每一个key对应的过期时间
     */
    private final Map<String, Pair<TimeUnit, Integer>> KEY_EXPIRE = new HashMap<>();

    private synchronized Cache<String, Object> buildCacheInstance(TimeUnit timeUnit, int expire) {
        String cacheId = timeUnit.name() + expire;
        return valueGetAsy(EXPIRE_CACHE, cacheId,
                () -> {
                    Caffeine<Object, Object> objectObjectCaffeine = Caffeine.newBuilder();
                    //小于等于0的就是永久存储的
                    if (expire >= 0) {
                        objectObjectCaffeine.expireAfterWrite(expire, timeUnit);
                    }
                    //将结果放入
                    Cache<String, Object> build = objectObjectCaffeine.build();
                    EXPIRE_CACHE.put(cacheId, build);
                    return build;
                });
    }

    @SuppressWarnings("unchecked")
    private <K, V> V valueGetAsy(Map<K, V> map, K key, Supplier<V> defaultValue) {
        Object object = map.get(key);
        if (object != null) {
            return (V) object;
        }
        return defaultValue.get();
    }

    @Override
    public String generateKey(String service, String method, Map<String, String> params, String[] excludes) {
        String result = service + "##" + method;
        if (params == null || params.size() == 0) {
            return result;
        }
        if (excludes.length > 0) {
            params.remove("sign");
            for (String exclude : excludes) {
                params.remove(exclude);
            }
            return result + params.values().stream().collect(Collectors.joining());
        } else {
            return result + params.values().stream().collect(Collectors.joining());
        }
    }

    @Override
    public String generateKey(String service, String method, String httpContent) {
        return service + "##" + method + "##" + httpContent;
    }

    @Override
    public Object get(String key) {
        Pair<TimeUnit, Integer> expireKey = KEY_EXPIRE.get(key);
        if (expireKey != null) {
            return buildCacheInstance(expireKey.getKey(), expireKey.getValue()).getIfPresent(key);
        }
        return null;
    }

    @Override
    public boolean set(String key, Object value, int expire, TimeUnit unit) {
        KEY_EXPIRE.put(key, new Pair<>(unit, expire));
        buildCacheInstance(unit, expire).put(key, value);
        return true;
    }
}
