package com.yunchang.spring.visitor.demo.model;

import com.yunchang.spring.visitor.core.invoker.DefaultServiceInvoker;
import com.yunchang.spring.visitor.demo.utils.JsonUtil;

import java.util.List;

/**
 * 自定义复杂对象类型(支持json字符串转该对象)
 * 必须提供一个无参数的构造函数。
 * Created by jasontujun on 2019/9/20.
 */
public class MyJsonObject implements DefaultServiceInvoker.TypeConverter<MyJsonObject> {
    private String id;
    private List<MyDataType> list;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<MyDataType> getList() {
        return list;
    }

    public void setList(List<MyDataType> list) {
        this.list = list;
    }

    @Override
    public MyJsonObject convert(String value) {
        return JsonUtil.toObject(value, MyJsonObject.class);
    }
}
