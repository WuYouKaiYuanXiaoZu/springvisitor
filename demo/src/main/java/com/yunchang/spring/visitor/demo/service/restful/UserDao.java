package com.yunchang.spring.visitor.demo.service.restful;

import com.yunchang.spring.visitor.core.dao.IDao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Dao层暂时用假数据实现。实际项目中，可以使用Hibernate、Mybatis等。
 * Created by duan on 2018/5/24.
 */
public class UserDao implements IDao<UserEntity> {

    @Override
    public Class<UserEntity> getEntityClass() {
        return UserEntity.class;
    }

    @Override
    public String getTableName() {
        return null;
    }

    @Override
    public String getIdFieldName() {
        return null;
    }

    @Override
    public String getIdFieldValue(UserEntity entity) {
        return null;
    }

    @Override
    public List<UserEntity> loadAll() {
        List list = new ArrayList<UserEntity>();
        list.add(new UserEntity("user1", 11));
        list.add(new UserEntity("user2", 22));
        list.add(new UserEntity("user3", 33));
        return list;
    }

    @Override
    public UserEntity load(Serializable id) {
        return new UserEntity("user8", 88);
    }

    @Override
    public UserEntity get(Serializable id) {
        return new UserEntity("user" + id, 99);
    }

    @Override
    public List<UserEntity> getByIds(List<Serializable> ids) {
        return null;
    }

    @Override
    public void save(UserEntity entity) {

    }

    @Override
    public void update(UserEntity entity) {
        entity.setName(entity.getName() + "_update");
    }

    @Override
    public void saveOrUpdate(UserEntity entity) {
        entity.setName(entity.getName() + "_saveor_update");
    }

    @Override
    public void saveAll(List<UserEntity> entities) {

    }

    @Override
    public void merge(UserEntity entity) {

    }

    @Override
    public void remove(UserEntity entity) {

    }

    @Override
    public void removeAll(List<UserEntity> entities) {

    }

    @Override
    public UserEntity findOne(String[] paramsName, Object... paramsValue) {
        return null;
    }

    @Override
    public List<UserEntity> findAll(String[] paramsName, Object... paramsValue) {
        return null;
    }

    @Override
    public List<UserEntity> findAll(Map<String, String> properties) {
        List list = new ArrayList<UserEntity>();
        list.add(new UserEntity("user7", 77));
        list.add(new UserEntity("user8", 88));
        list.add(new UserEntity("user9", 99));
        return list;
    }

    @Override
    public List<UserEntity> findAllByPage(int start, int pageSize,
                                          String sortKey, boolean isASC,
                                          Map<String, String> properties) {
        return null;
    }
}
