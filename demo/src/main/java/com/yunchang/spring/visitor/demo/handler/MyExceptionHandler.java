package com.yunchang.spring.visitor.demo.handler;

import com.yunchang.spring.visitor.core.handler.exception.IExceptionHandler;
import com.yunchang.spring.visitor.core.utils.WebUtil;
import com.yunchang.spring.visitor.demo.utils.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * 自定义异常处理器示例：统一以Map形式返回httpResponse
 * Created by jasontujun on 2018/1/16.
 */
@Component
public class MyExceptionHandler implements IExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Object handleError(HttpServletRequest request, HttpServletResponse response,long costTime,
                              Throwable exception, Map<String, String> params, String httpContent) {
        // 统一返回内容为json格式，成功status为ok，失败status为fail
        Map<String, Object> resMap = new HashMap<String, Object>();
        resMap.put("status", "fail");
        String errorMsg;
        if (exception instanceof InvocationTargetException) {
            errorMsg = ((InvocationTargetException) exception).getTargetException().getMessage();
        } else {
            errorMsg = exception.getMessage();
        }
        resMap.put("error", errorMsg);
//        if (logger.isInfoEnabled()) {
            Map<String, Object> logMap = new HashMap<String, Object>();
            logMap.put("time", System.currentTimeMillis());
            logMap.put("uri", WebUtil.getUri(request));
            logMap.put("ip", WebUtil.getIpAddr(request));
            logMap.put("params", params);
            logMap.put("httpContent", httpContent);
            logMap.put("response", resMap);
//            logger.info(JsonUtil.toJson(logMap));
            System.out.println(JsonUtil.toJson(logMap));
//        }
        response.setHeader("Connection", "close");
        response.setContentType("application/json;charset=UTF-8");
        return resMap;
    }

    @Override
    public String response2string(Object response) {
        return JsonUtil.toJson(response);
    }
}
