package com.yunchang.spring.visitor.demo.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

/**
 * 对ITestService的AOP切面
 * Created by jasontujun on 2018/11/21.
 */
@Order(1)
@Aspect
public class TestAspect {

    @Before(value = "execution(* com.yunchang.spring.visitor.demo.service.ITestService.*(..))")
    public void beforeTest(JoinPoint joinPoint) throws Throwable {
        // 测试切面逻辑，会被正常调用
        System.out.println("test aop!");
    }

    @Before(value = "execution(* com.yunchang.spring.visitor.demo.service.IDownloadService.*(..))")
    public void beforeDownload(JoinPoint joinPoint) throws Throwable {
        // 测试切面逻辑，会被正常调用
        System.out.println("test download!");
    }
}
