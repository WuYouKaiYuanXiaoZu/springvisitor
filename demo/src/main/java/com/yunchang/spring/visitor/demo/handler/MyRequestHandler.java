package com.yunchang.spring.visitor.demo.handler;

import com.yunchang.spring.visitor.core.constant.ContentType;
import com.yunchang.spring.visitor.core.handler.request.ABaseRequestHandler;
import com.yunchang.spring.visitor.demo.utils.JsonUtil;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 自定义请求入参处理器示例
 * Created by jasontujun on 2019/9/20.
 */
@Component
public class MyRequestHandler extends ABaseRequestHandler {

    @Override
    public Map<String, String> parseToMap(String httpContent, ContentType contentType) throws Exception {
        if (contentType == ContentType.JSON) {
            return JsonUtil.getMap(httpContent);
        } else {
            return null;
        }
    }
}
