package com.yunchang.spring.visitor.demo.controller;

import com.yunchang.spring.visitor.core.controller.ARestfulController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 测试Restful风格接口的Controller
 * Created by jasontujun on 2018/1/11.
 */
@Controller
public class TestRestfulController extends ARestfulController {

    @RequestMapping(value = "/rest/user")
    @ResponseBody
    public Object doTest(final HttpServletRequest request, final HttpServletResponse response) {
        return invokeService(request, response, "rest", "user");
    }

    @RequestMapping("/rest/user/{id}")
    @ResponseBody
    public Object doTest(@PathVariable final String id,
                         final HttpServletRequest request, final HttpServletResponse response) {
        return invokeService(request, response, "rest", "user", id);
    }
}
