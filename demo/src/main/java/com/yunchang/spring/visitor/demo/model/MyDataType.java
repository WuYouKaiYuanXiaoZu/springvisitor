package com.yunchang.spring.visitor.demo.model;

import com.yunchang.spring.visitor.core.invoker.DefaultServiceInvoker;

/**
 * 自定义数据类型<br/>
 * 实现DefaultServiceInvoker.TypeConverter接口，将String转换为该类型的对象。
 * 必须提供一个无参数的构造函数。
 * Created by jasontujun on 2018/5/15.
 */
public class MyDataType implements DefaultServiceInvoker.TypeConverter<MyDataType> {

    private String name;
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public MyDataType convert(String value) {
        // 接口层，参数是"name,value"的格式，调用到service自动转换成MyDataType对象
        String str[] = value.split(",");
        MyDataType entity = new MyDataType();
        if (str.length == 2) {
            entity.setName(str[0]);
            entity.setValue(str[1]);
            return entity;
        } else {
            return null;
        }
    }
}
