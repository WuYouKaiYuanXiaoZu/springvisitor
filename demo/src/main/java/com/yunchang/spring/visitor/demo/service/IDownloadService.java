package com.yunchang.spring.visitor.demo.service;

import com.yunchang.spring.visitor.core.annotation.ReflectiveMethod;
import com.yunchang.spring.visitor.core.annotation.ReflectiveService;
import com.yunchang.spring.visitor.core.handler.response.DownloadHandler;

import java.util.Map;

/**
 * 下载接口
 * Created by jasontujun on 2018/11/16.
 */
@ReflectiveService(value = "service", namespace = "download")
public interface IDownloadService {
    /**
     * 反射调用该方法，提供文件下载
     * 测试链接 {@link}http://127.0.0.1:8080/download/service/file?name=download.txt
     *
     * @see DownloadHandler
     */
    @ReflectiveMethod
    Map file(Map<String, String> params);
}
