package com.yunchang.spring.visitor.demo.service.restful;

import com.yunchang.spring.visitor.core.annotation.Params;
import com.yunchang.spring.visitor.core.annotation.ReflectiveService;
import com.yunchang.spring.visitor.core.annotation.Restful;
import com.yunchang.spring.visitor.core.dao.IDao;
import com.yunchang.spring.visitor.core.service.ARestfulService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 示例：用户数据的增删改查接口。
 * http://127.0.0.1:8080/rest/user  GET
 * http://127.0.0.1:8080/rest/user/12  GET  操作拒绝
 * http://127.0.0.1:8080/rest/user  POST  参数 name=wang & age=12 & test=test123   只返回name字段
 * http://127.0.0.1:8080/rest/user/12  POST
 * <p>
 * Created by duan on 2018/5/24.
 */
@Service
@ReflectiveService(value = "user", namespace = "rest",
        output = {@Params(methodName = "list", params = {"name"})},
        input = {@Params(methodName = "add", params = {"name"})})
@Restful(get = false)
public class UserRestfulService extends ARestfulService {
    @Override
    protected IDao getDao() {
        return new UserDao();
    }

    @Override
    public Map add(Map properties) throws Exception {
        return properties;
    }
}
